<?php

namespace App\Http\Controllers\BookManagement;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all()->sortByDesc('year');
        return view('bookmanagement.index')->with(compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all()->sortBy('name');
        return view('bookmanagement.create')->with(compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // rules validator
        $validate = $request->validate([
            'title'         => ['required', 'string', 'max:255'],
            'description'   => ['nullable', 'string', 'max:255'],
            'author'        => ['required', 'string', 'max:255'],
            'year'          => ['required', 'numeric'],
            'category_id'   => ['required', 'numeric'],
        ]);

        try {
            // create new book
            $book = Book::create([
                'category_id'   => $request->category_id,
                'title'         => $request->title,
                'description'   => $request->description,
                'author'        => $request->author,
                'year'          => $request->year,
            ]);
        } catch (\Throwable $th) {
            report($th);
            return redirect()->route('books.index')
            ->with('error', 'Something wrong, Book fail to create');
        }

        return redirect()->route('books.index')
            ->with('success', 'Book created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return view('bookmanagement.show')->with(compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        $categories = Category::all()->sortBy('name');
        return view('bookmanagement.edit')->with(compact('book', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        // rules validator
        $validate = $request->validate([
            'category_id'   => ['required', 'numeric'],
            'title'         => ['required', 'string', 'max:255'],
            'description'   => ['nullable', 'string', 'max:255'],
            'author'        => ['required', 'string', 'max:255'],
            'year'          => ['required', 'numeric'],
        ]);

        try {
            $book->update($request->all());
        } catch (\Throwable $th) {
            report($th);
            return redirect()->route('books.index')
            ->with('error', 'Something wrong, Book fail to update');
        }

        return redirect()->route('books.index')
            ->with('success', 'Book updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        try {
            $book->delete();
        } catch (\Throwable $th) {
            report($th);
            return redirect()->route('books.index')
            ->with('error', 'Something wrong, Book fail to delete');
        }

        return redirect()->route('books.index')
            ->with('success', 'Book deleted successfully.');
    }
}
