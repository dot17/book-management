<?php

namespace App\Http\Controllers\Api\BookManagement;

use App\Http\Controllers\Controller;
use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /* Getting the request parameters. */
        $detail = $request->detail;
        $search = $request->search;

        /* A query builder. */
        $books = (new Book)->newQuery();
        if ($request->search) {
            $books->where('title', 'LIKE', "%{$search}%")
                ->orWhere('author', 'LIKE', "%{$search}%")
                ->orWhere('year', 'LIKE', "%{$search}%");
        }
        if ($detail == "true") {
            $books->with('category');
        }
        $books = $books->get();

        /* This is a conditional statement that checks if the books are not found. If the
        books are not found, it returns a response with a 404 status code. */
        if(!count($books)){
            return response()->json([
                'success'   => false,
                'message'   => 'Books not found',
                'data'      => [],
            ], 404);
        }

        /* Returning a JSON response. */
        return response()->json([
            'success'   => true,
            'message'   => 'Books fetched successfully',
            'data'      => $books,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /* Finding the book with the given id. */
        $book = Book::with('category')->find($id);

        /* This is a conditional statement that checks if the book is not found. If the book is
        not found, it returns a response with a 404 status code. */
        if (empty($book)) {
            return response()->json([
                'success'   => false,
                'message'   => 'Book not found',
                'data'      => (object)[],
            ], 404);
        }

        /* Returning a JSON response. */
        return response()->json([
            'success'   => true,
            'message'   => 'Book fetched successfully',
            'data'      => $book,
        ]);
    }
}
