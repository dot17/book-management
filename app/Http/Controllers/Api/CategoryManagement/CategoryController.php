<?php

namespace App\Http\Controllers\Api\CategoryManagement;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /* Getting the request parameters. */
        $detail = $request->detail;
        $search = $request->search;

        /* A query builder. */
        $categories = (new Category)->newQuery();
        if ($request->search) {
            $categories->where('name', 'LIKE', "%{$search}%");
        }
        if ($detail == "true") {
            $categories->with('books');
        }
        $categories = $categories->get();

        /* This is a conditional statement that checks if the categories are not found. If the
        categories are not found, it returns a response with a 404 status code. */
        if(!count($categories)){
            return response()->json([
                'success'   => false,
                'message'   => 'Categories not found',
                'data'      => [],
            ], 404);
        }

        /* Returning a JSON response. */
        return response()->json([
            'success'   => true,
            'message'   => 'Categories fetched successfully',
            'data'      => $categories,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /* Finding the category with the given id. */
        $category = Category::with('books')->find($id);

        /* This is a conditional statement that checks if the category is not found. If the category is
        not found, it returns a response with a 404 status code. */
        if (empty($category)) {
            return response()->json([
                'success'   => false,
                'message'   => 'Category not found',
                'data'      => (object)[],
            ], 404);
        }

        /* Returning a JSON response. */
        return response()->json([
            'success'   => true,
            'message'   => 'Category fetched successfully',
            'data'      => $category,
        ]);
    }
}
