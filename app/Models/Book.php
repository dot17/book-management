<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'category_id',
        'title',
        'description',
        'author',
        'year',
    ];

    /**
     * The `category()` function returns the category that the product belongs to
     *
     * @return The category that the post belongs to.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}
