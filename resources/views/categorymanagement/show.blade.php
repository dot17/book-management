@extends('layouts.layout')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-xl-8 col-lg-12 col-sm-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-header">
                        <h3>Category Detail</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input name="name" type="text" class="form-control" id="" placeholder="Name"
                                    value="{{ $category->name }}" readonly="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label">Description</label>
                            <div class="col-sm-9">
                                <textarea name="description" class="form-control" id="" cols="30" rows="4" readonly>{{ $category->description }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label">Books</label>
                            <div class="col-sm-9">
                                <div class="table-responsive">
                                    <table id="example" class="table table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Title</th>
                                                <th>Author</th>
                                                <th>Year</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($category->books as $book)
                                                <tr>
                                                    <td>{{ $loop->index + 1 }}</td>
                                                    <td> {{ $book->title }} </td>
                                                    <td> {{ $book->author }} </td>
                                                    <td> {{ $book->year }} </td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="4">
                                                        <center>Empty Data</center>
                                                    </td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row float-right">
                            <a href="{{ route('categories.index') }}" class="btn btn-secondary mr-2">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        })
    </script>
@stop
