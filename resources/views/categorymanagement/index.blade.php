@extends('layouts.layout')

@section('content')
    <div class="content-wrapper">
        @if ($message = Session::get('success'))
            <div class="alert alert-dismissable alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <span>{{ $message }}</span>
            </div>
        @elseif ($message = Session::get('error'))
            <div class="alert alert-dismissable alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <span>{{ $message }}</span>
            </div>
        @endif

        <div class="row">
            <div class="col-12 grid-margin">
                <div class="card">
                    <div class="card-header">
                        <h3>Category List</h3>
                    </div>
                    <div class="card-body">
                        <a class="btn btn-sm btn-gradient-primary" href="{{ route('categories.create') }}"><i
                                class="mdi mdi-plus"></i> Create</a>
                        <div class="table-responsive mt-4">
                            <table id="example" class="table table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($categories as $category)
                                        <tr>
                                            <td>{{ $loop->index + 1 }}</td>
                                            <td>{{ $category->name }}</td>
                                            <td>{{ $category->description }}</td>
                                            <td>
                                                <form action="{{ route('categories.destroy', $category->id) }}"
                                                    method="POST">
                                                    <div class="btn-group">
                                                        <a class="btn btn-sm btn-info view_modal color"
                                                            href="{{ route('categories.show', $category->id) }}"><i
                                                                class="mdi mdi-eye"></i></a>
                                                        <a class="btn btn-sm btn-warning edit_modal color"
                                                            href="{{ route('categories.edit', $category->id) }}"><i
                                                                class="mdi mdi-grease-pencil"></i></a>
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-sm btn-danger delete color"
                                                            onclick="return confirm('You will delete all data that relate to this?');"><i
                                                                class="mdi mdi-delete"></i></button>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4">
                                                <center>Empty Data</center>
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        })
    </script>
@stop
