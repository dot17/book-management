@extends('layouts.layout')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-xl-8 col-lg-12 col-sm-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-header">
                        <h3>Category Edit</h3>
                    </div>
                    <div class="card-body">
                        <p class="card-description">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </p>
                        <form class="forms-sample" action="{{ route('categories.update', $category->id) }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label">Device Name</label>
                                <div class="col-sm-9">
                                    <input name="name" type="text" class="form-control" id=""
                                        placeholder="Name" value="{{ $category->name }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label">Description</label>
                                <div class="col-sm-9">
                                    <textarea name="description" class="form-control" id="" cols="30" rows="4"
                                        placeholder="Lorem ipsum, dolor sit amet consectetur adipisicing elit. Earum libero veritatis sint aspernatur quia iste, ratione nihil explicabo maiores deserunt enim error eius dolorum sunt dolor dicta beatae aliquid saepe!">{{ $category->description }}</textarea>
                                </div>
                            </div>
                            <div class="row float-right">
                                <a href="{{ route('categories.index') }}" class="btn btn-secondary mr-2">Cancel</a>
                                <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
