@extends('layouts.layout')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-xl-8 col-lg-12 col-sm-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-header">
                        <h3>Book Detail</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label">Title</label>
                            <div class="col-sm-9">
                                <input name="title" type="text" class="form-control" id="" placeholder="Name"
                                    value="{{ $book->title }}" readonly="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label">Description</label>
                            <div class="col-sm-9">
                                <textarea name="description" class="form-control" id="" cols="30" rows="4" readonly>{{ $book->description }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label">Author</label>
                            <div class="col-sm-9">
                                <input name="author" type="text" class="form-control" id=""
                                    placeholder="John Doe" value="{{ $book->author }}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label">Year</label>
                            <div class="col-sm-9">
                                <input name="year" type="number" class="form-control" id="" placeholder="2000"
                                    value="{{ $book->year }}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label">Category</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="category_id">
                                    <option value="{{ $book->category->id }}" disabled selected>{{ $book->category->name }}
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="row float-right">
                            <a href="{{ route('books.index') }}" class="btn btn-secondary mr-2">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
