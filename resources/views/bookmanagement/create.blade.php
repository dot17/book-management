@extends('layouts.layout')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-xl-8 col-lg-12 col-sm-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-header">
                        <h3>Create New Book</h3>
                    </div>
                    <div class="card-body">
                        <p class="card-description">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </p>
                        <form class="forms-sample" action="{{ route('books.store') }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label">Title</label>
                                <div class="col-sm-9">
                                    <input name="title" type="text" class="form-control" id=""
                                        placeholder="Lorem ipsum dolor sit amet" value="{{ old('title') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label">Description</label>
                                <div class="col-sm-9">
                                    <textarea name="description" class="form-control" id="" cols="30" rows="4"
                                        placeholder="Lorem ipsum, dolor sit amet consectetur adipisicing elit. Earum libero veritatis sint aspernatur quia iste, ratione nihil explicabo maiores deserunt enim error eius dolorum sunt dolor dicta beatae aliquid saepe!">{{ old('description') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label">Author</label>
                                <div class="col-sm-9">
                                    <input name="author" type="text" class="form-control" id=""
                                        placeholder="John Doe" value="{{ old('author') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label">Year</label>
                                <div class="col-sm-9">
                                    <input name="year" type="number" class="form-control" id=""
                                        placeholder="2000" value="{{ old('year') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label">Category</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="category_id">
                                        @foreach ($categories as $category)
                                            {{-- <option value="{{ $category->id }}">{{ $category->name }}
                                            </option> --}}
                                            <option value="{{ $category->id }}"
                                                {{ $category->id == old('category_id') ? 'selected' : '' }}>
                                                {{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row float-right">
                                <a href="{{ route('books.index') }}" class="btn btn-secondary mr-2">Cancel</a>
                                <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
