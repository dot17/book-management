# About Projects

Book.you is a basic web management project based on [Laravel](https://laravel.com/) that can be used to manage books mainly for Library/Bookstore. In addition, this project also has endpoints to fetch and search books that already submitted.

# Database Design

![Database Diagram](https://drive.google.com/uc?export=view&id=1qQd-8cM5gfCi64QO5UQkOCcSa52E80LH)

# Screenshots

## Welcome Page & Login Page

![Welcome Page](https://drive.google.com/uc?export=view&id=1fYrtbfoereNKf7XUOsr-4Gp7F_KIDyud)
![Login Page](https://drive.google.com/uc?export=view&id=1Ez9XxjyThmfoSx08fR3_yAWut8jkNPm2)

## Category Management

![Category List](https://drive.google.com/uc?export=view&id=16yFeVqxZzLHLOnCecTBBCFOudRAPL6jg)
![Category Create](https://drive.google.com/uc?export=view&id=1OmJTnVhlRrACKpkJxmgCEZzKO1_YQQUo)
![Category Read](https://drive.google.com/uc?export=view&id=1aYvbrmzITJftVT2ivETB7eExvGpanypu)
![Category Update](https://drive.google.com/uc?export=view&id=1GlnP_cgrxehGeOl6Q9gDWN_5lnew5A7S)

## Book Management

![Book List](https://drive.google.com/uc?export=view&id=1TsemGvyeX110R6Exn-Z3idwKIX0Qg4xj)
![Book Create](https://drive.google.com/uc?export=view&id=1iTmAC1eCHKxsn_SYORu4U8XfsWmc_B4r)
![Book Read](https://drive.google.com/uc?export=view&id=1-5xWqNJPYgjHJ2QlYIuDzAckz3g-LfQR)
![Book Update](https://drive.google.com/uc?export=view&id=1quMGHufq-KAKZL7xmJ1KxsVXWqpjuW89)

# Dependencies

-   [Laravel](https://laravel.com/) v8.83.23
-   [Composer](https://getcomposer.org/) v2.3.10
-   [PHP](https://www.php.net/) v7.4.30
-   [MySQL](https://www.mysql.com/) v8.0.30
-   [Postman](https://www.postman.com/) v9.27.2
-   [Laravel Valet](https://laravel.com/docs/9.x/valet) v2.18.10

# Other Information

## How to Install

1. Clone this project to local
2. Prepare a new database
3. Rename `.env.example` in root project directory to `.env`
4. Open `.env` and adjust this section with existing database

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```

5. Open Command Prompt/Terminal to project directory and type this in order:
    - `composer install` to Install composer for the project
    - `composer update` to Update composer for the project (\*)optional
    - `php artisan migrate` to Migrate(creating table and field) database
    - `php artisan db:seed` to Seed(inserting data) database with dummy data
    - `php artisan key:generate` to Generate Laravel key

## How to Run in Web

1. Open Command Prompt/Terminal to project directory and type this:

```
php artisan serve
```

2. Open Browser and type `localhost:8000` as URL
3. You can Register or Login using an existing account:
    - Email: `example@database.com`
    - Password: `password`

## How to Run API

1. Open Command Prompt/Terminal to project directory and type this:

```
php artisan serve
```

2. Open Postman and type URL
    - Use `GET` and `localhost:8000/api/categories` to get all categories data
    - Use `GET` and `localhost:8000/api/books` to get all books data
3. Click [here](https://www.postman.com/msyahruls/workspace/my-public-workspace/documentation/1475503-e4d84fca-f964-476a-9173-2e351b20958e?entity=folder-13e5c6c4-d448-4603-88c7-0e9ab7c53d9c) to see more about endpoints documentation.
